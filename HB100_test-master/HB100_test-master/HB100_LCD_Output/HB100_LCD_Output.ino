/* FreqMeasure - Example with LCD output
   http://www.pjrc.com/teensy/td_libs_FreqMeasure.html

   Slightly modified by @3zuli for use with HB100 doppler radar module

   This example code is in the public domain.

   Frequency input pin: 8. Connect the IF pin from the preamp board to this pin.
   This should work with Arduino Uno, Nano, Pro mini and simillar.
   For other boards, see the FreqMeasure documentation (link above).

   16x2 character display is connected exactly as in the LiquidCrystal library examples:
   LCD RS pin to digital pin 12
   LCD Enable pin to digital pin 11
   LCD D4 pin to digital pin 5
   LCD D5 pin to digital pin 4
   LCD D6 pin to digital pin 3
   LCD D7 pin to digital pin 2
   LCD R/W pin to ground
   10K resistor:
   ends to +5V and ground
   wiper to LCD VO pin (pin 3)
*/


#include <FreqMeasure.h>

void setup() {

  Serial.begin(9600);
  Serial.print("Freq:");
  Serial.print("    ");
  Serial.println("Speed:");
  FreqMeasure.begin();
}

double sum = 0;
int count = 0;
int avgdivisor = 5;
double firstFreq = 0;
double lastFreq = 0;
double direct = 0;
double frequency = 0;

void loop() {
  if (FreqMeasure.available()) {
    Serial.println("here");
    // average 30 readings together
    frequency = FreqMeasure.read();
    if (FreqMeasure.countToFrequency(frequency) > 0.7)
    {
      sum = sum + frequency;
      if (count == 1) {
        firstFreq = frequency;
      } else if (count == floor(avgdivisor / 2)) {
      lastFreq = frequency;
    }
    count = count + 1;
  }
}

if (count > avgdivisor) {
    float frequency = FreqMeasure.countToFrequency(sum / count);
    float spd = frequency / 19.49; //conversion from frequency to kilometers per hour (sorry, imperial guys)
    
    Serial.print(frequency);
    Serial.print("Hz");
    Serial.print("    ");
    Serial.print(spd);
    Serial.print("km/h");
    Serial.println("    ");


    sum = 0;
    count = 0;
  }
}
