//library for TTN lora
#include <TheThingsNetwork.h>
//Timer interrupt function library
#include <MsTimer2.h>

// Set your DevAddr, NwkSKey, AppSKey and the frequency plan
const char *devAddr = "26041FA8";
const char *nwkSKey = "6942E54BF7B3CAFA18B2D9250FA87418";
const char *appSKey = "5436B660176710D044E7899100264FBD";

// define variables for lora
#define loraSerial Serial1
#define debugSerial Serial

// Replace REPLACE_ME with TTN_FP_EU868 or TTN_FP_US915
#define freqPlan TTN_FP_US915

// array used to store the bytes to be sent over ttn
byte payload[20];
int nextByte = 0;

TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

//defining pins for motion sensor
#define trigPin 6
#define echoPin 5
// Define interrupt 1 that is digital pin 3
int pbIn = 1;
//Interrupt times
int number = 0;
// Defines the indicator LED state, the default is not bright
volatile int state = LOW;
// Value used to resest the value of count
const int countValue = 100;
// how many loops to wait before sending data
int count = countValue;

//variable to stroe the current distance reading
int distance;
//variable to store the last maximum distance reading
double maxDistance = 0;
//buffer distance to allow for inacurate sensor reading
int tolerance = 20;

//array used to store 5 readings that are averaged together
int tmp[5];
int tmpNext = 0;

//array used to store all the averaged distance readings
int list[30] = {0};
int next = 0;

void setup()
{
  loraSerial.begin(57600);
  debugSerial.begin(9600);

  // Wait a maximum of 10s for Serial Monitor
  while (!debugSerial && millis() < 10000)
    ;

  debugSerial.println("-- PERSONALIZE");
  ttn.personalize(devAddr, nwkSKey, appSKey);

  debugSerial.println("-- STATUS");
  ttn.showStatus();

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  attachInterrupt(pbIn, stateChange, FALLING); // Set the interrupt function, interrupt pin is digital pin D2, interrupt service function is stateChange (), when the D2 power change from high to low , the trigger interrupt.
  MsTimer2::set(100, Handle); // Set the timer interrupt function, running once Handle() function per 100ms
  MsTimer2::start();//Start timer interrupt function
}

void loop()
{
  if (count != 0) //if there has been motion within the last countValue loops take distance reading
  {
//    Serial.println(count);
    //take a distance reading
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    //Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds.
    distance = ((pulseIn(echoPin, HIGH) / 2) / 29);
    //check distance is less than max distance with 25mm tolerance
    if (distance < maxDistance - tolerance && next < 31) {
      //take distance reading if tmpNext = 4 take next reading and average all 5 values and add to list
      if (tmpNext < 4) {
        tmp[tmpNext] = distance;
        tmpNext++;
      } else {
        Serial.println(next);
        tmp[tmpNext] = distance;
        int averageVal = (tmp[0] + tmp[1] + tmp[2] + tmp[3] + tmp[4]) / 5;
        list[next] = averageVal;
        next++;
        tmpNext = 0;
        //if the average value is larger than the max distance set maxDistance = avgVal
        if (averageVal > maxDistance) {
          if (maxDistance - averageVal < 100) {
            maxDistance = averageVal;
          }
        }
      }
    }
    //used to initaly set maxDistance
    if (maxDistance == 0) {
      maxDistance = distance;
    }
    //decrement count to track number of loops
    count--;
  }
  else {
    if(next != 0 && count <= 1){
      digitalWrite(LED_BUILTIN, HIGH);
      Serial.println("SEND 10");
      //send 0 -> 9 elements of list arr over TTN
      for (int i = 0; i < 10 ; i++) {
        // Prepare payload of bytes
        payload[nextByte] = highByte(list[i]);
        nextByte++;
        payload[nextByte] = lowByte(list[i]);
        nextByte++;
        if (i == 9) {
          // Send it off
          ttn.sendBytes(payload, 20);
          nextByte = 0;
        }
      }

      Serial.println("SEND 20");
      //send 10 -> 19 elements of list arr over TTN
      for (int i = 0; i < 10 ; i++) {
        // Prepare payload of bytes
        payload[nextByte] = highByte(list[i + 10]);
        nextByte++;
        payload[nextByte] = lowByte(list[i + 10]);
        nextByte++;
        if (i == 9) {
          // Send it off
          ttn.sendBytes(payload, 20);
          nextByte = 0;
        }
      }

      Serial.println("SEND 30");
      //send 20 -> 29 elements of list arr over TTN
      for (int i = 0; i < 10 ; i++) {
        // Prepare payload of bytes
        payload[nextByte] = highByte(list[i + 20]);
        nextByte++;
        payload[nextByte] = lowByte(list[i + 20]);
        nextByte++;
        if (i == 9) {
          // Send it off
          ttn.sendBytes(payload, 20);
          nextByte = 0;
        }
      }
  
      //reset all list values to 0
      memset(list, 0, sizeof list);
  
      //set next value to be added to list to 0
      next = 0;
      digitalWrite(LED_BUILTIN, LOW);
    }

  }
}

void stateChange()  //Interrupt service function
{
  number++;  //Interrupted once, the number +1
  count = countValue;
  Serial.println("INTERUPT");
}

void Handle()   //Timer service function
{
  if (number > 1) //If in the set of the interrupt time the number more than 1 times, then means have detect moving objects,This value can be adjusted according to the actual situation, which is equivalent to adjust the threshold of detection speed of moving objects.
  {
    number = 0; //Cleare the number, so that it does not affect the next trigger
  }
  else
    number = 0; //If in the setting of the interrupt time, the number of the interrupt is not reached the threshold value, it is not detected the moving objects, Cleare the number.
}
