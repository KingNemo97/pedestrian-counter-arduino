// Based on the Adafruit Trinket Sound-Reactive LED Color Organ
// http://learn.adafruit.com/trinket-sound-reactive-led-color-organ/code

#define RADAR A7 // RADAR inut is attached to A7
#define MICRODELAY 100 // 100microseconds ~10000hz
#define MAXINDEX 1024 // 10 bits
#define TOPINDEX 1023 // 10 bits

byte collect[MAXINDEX];
int mean;
int minimum;
int maximum;
int hysteresis; // 1/16 of max-min
bool currentphase; // are value above mean + hysteresis;
int lastnull; // index for last null passing value
int prevnull; // index for previous null passing value
int deltaindex;
int deltadeltaindex;
int index;
bool phasechange = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial) {}
  index = 0;
  mean = 0;
  maximum = 255;
  minimum = 0;
  hysteresis = 0;
  currentphase = false;
  lastnull = 0;
  prevnull = 0;

  Serial.print("deltadeltaindex");
  Serial.print("\t");
  Serial.print("deltaindex");
  Serial.print("\t");
  Serial.println("delta");
}

void loop() {
  int newVal = analogRead(RADAR); // Raw reading from amplified radar
  mean -= (collect[index] >> 2);
  mean += (newVal >> 2);
  collect[index]= newVal;
  minimum = newVal < minimum ? newVal : minimum + 1;
  maximum = newVal > maximum ? newVal : maximum - 1;
  hysteresis = abs(maximum - minimum) >> 5;

  if(newVal > (mean + hysteresis))
  {
    if(false == currentphase)
    {
      currentphase = true;
      phasechange = true;
    }
  }
  else if(newVal < (mean - hysteresis))
  {
    if(currentphase)
    {
      currentphase = false;      
      phasechange = true;
    }
  }

  if(phasechange)
  {
    prevnull = lastnull;
    lastnull = index;

    int delta = (prevnull > lastnull) ? 
        (lastnull - prevnull + MAXINDEX) : 
        (lastnull - prevnull);
    deltadeltaindex = abs(deltaindex - delta);
    deltaindex = delta;

    Serial.print(deltadeltaindex);
    Serial.print("\t");
    Serial.print(deltaindex);
    Serial.print("\t");
    Serial.println(delta);
  }
  
  index = index == TOPINDEX ? 0 : index + 1;
  phasechange = false;
  //delayMicroseconds(10);
}
