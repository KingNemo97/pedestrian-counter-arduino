/* FreqMeasure - Example with LCD output
   http://www.pjrc.com/teensy/td_libs_FreqMeasure.html

   Slightly modified by @3zuli for use with HB100 doppler radar module

   This example code is in the public domain.

   Frequency input pin: 8. Connect the IF pin from the preamp board to this pin.
   This should work with Arduino Uno, Nano, Pro mini and simillar.
   For other boards, see the FreqMeasure documentation (link above).
*/

//doppler related variables
#include <FreqMeasure.h>

double sum = 0;
int count = 0;
const int avgdivisor = 5;
double frequency = 0;

//distance related variables
#define trigPin 2
#define echoPin 3
//Define variables
long distance = 0;
int averageDist = 0;
int distanceValues[avgdivisor];
long distanceTime; // value used to store the time when the next direction will be printed out
boolean printDist = false;
int distCount = 0;

void setup() {

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  Serial.begin(9600);
  Serial.print("Freq:");
  Serial.print("    ");
  Serial.print("Speed:");
  FreqMeasure.begin();
}

void loop() {
  if (FreqMeasure.available()) {
    frequency = FreqMeasure.read();
    if (FreqMeasure.countToFrequency(frequency) / 19.49 > 0.7) {
      sum = sum + frequency;

      //triggering distance sensor
      digitalWrite(trigPin, LOW);
      //Trigger the sensor by setting the trigPin high for 10 microseconds
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin, LOW);
      //Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds.
      distance = pulseIn(echoPin, HIGH);
//      Serial.println(distance);
      // adding distance value to distanceValues array if more then 2 seconds away from avg distance
      if (averageDist - distance > 3000 && distanceValues[avgdivisor - 1] == 0) {
        distanceValues[distCount] = distance;
        distCount++;
        printDist = true;
      } else {
        averageDist += distance;
        averageDist = averageDist / 2;
      }
      count += 1;
      distanceTime = millis() + 2000;
    }
  }

  if (count > avgdivisor) {
    float frequency = FreqMeasure.countToFrequency(sum / count);
    float spd = frequency / 19.49; //conversion from frequency to kilometers per hour

    Serial.print(frequency);
    Serial.print("Hz");
    Serial.print("    ");
    Serial.print(spd);
    Serial.print("km/h");
    Serial.println("    ");

    sum = 0;
    count = 0;
  }
  
  if (abs(abs(distanceTime) - millis()) < 1000 && printDist) {
    if (distanceValues[1] - distanceValues[2] < 0) {
      Serial.println("away");
    } else {
      Serial.println("towards");
    }
    
    printDist = false;
    distCount = 0;
    
//    for(int i = 0; i < avgdivisor; i++){
//      Serial.println(distanceValues[i]);
//    }
    
    for(int i = 0; i < avgdivisor; i++){
      distanceValues[i] = 0;
    }
  }
}
