#include <MsTimer2.h>           //Timer interrupt function library

int pbIn = 1;                    // Define interrupt 0 that is digital pin 2
int number = 0;                  //Interrupt times
volatile int state = LOW;         // Defines the indicator LED state, the default is not bright
int count = 0;

#define trigPin 6
#define echoPin 5
int distance;
const double maxDistance = 1000;
int tolerance = 10;

int tmp[5];
int tmpNext = 0;

int list[30];
int next = 0;

void setup()
{
  Serial.begin(9600);
  

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  attachInterrupt(pbIn, stateChange, FALLING); // Set the interrupt function, interrupt pin is digital pin D2, interrupt service function is stateChange (), when the D2 power change from high to low , the trigger interrupt.
  MsTimer2::set(100, Handle); // Set the timer interrupt function, running once Handle() function per 100ms
  MsTimer2::start();//Start timer interrupt function

Serial.println();
  Serial.println();
  Serial.println("start");

}

void loop()
{
  if (true) //When a moving object is detected, the ledout is automatically closed after the light 2S, the next trigger can be carried out, and No need to reset. Convenient debugging.
  {
    //take a distance reading
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    //Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds.
    distance = ((pulseIn(echoPin, HIGH) / 2) / 29);
    //check distance is less than max distance with 25mm tolerance
    if (distance < maxDistance - tolerance && next < 495) {
      
      if (tmpNext < 4) {
        tmp[tmpNext] = distance;
        tmpNext++;
      } else {
        tmp[tmpNext] = distance;
        int averageVal = (tmp[0] + tmp[1] + tmp[2] + tmp[3] + tmp[4]) / 5;
        list[next] = averageVal;
        Serial.println(averageVal);
        next++;
        tmpNext = 0;
        if (averageVal > maxDistance) {
          if (maxDistance - averageVal < 100) {
            //maxDistance = averageVal;
          }
        }
      }
    }
    /*if (maxDistance == 0) {
      maxDistance = distance;
    }*/
    count--;
  }
  else {
    for (int i = 0; i < next ; i++) {
      if(i == 0){
      }
      Serial.println(list[i]);
      if (i == next - 1) {
        Serial.println("END");
      }

    }
    next = 0;

  }
}

void stateChange()  //Interrupt service function
{
  number++;  //Interrupted once, the number +1
  count = 50;
}

void Handle()   //Timer service function
{
  if (number > 1) //If in the set of the interrupt time the number more than 1 times, then means have detect moving objects,This value can be adjusted according to the actual situation, which is equivalent to adjust the threshold of detection speed of moving objects.
  {
    number = 0; //Cleare the number, so that it does not affect the next trigger
  }
  else
    number = 0; //If in the setting of the interrupt time, the number of the interrupt is not reached the threshold value, it is not detected the moving objects, Cleare the number.
}
